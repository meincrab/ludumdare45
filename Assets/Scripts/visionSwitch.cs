﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class visionSwitch : MonoBehaviour
{

    public GameObject whiteDummy;
    private bool isFading;
    private Color alphaColor;
    private float timeToFade = 1.0f;
    void Start()
    {
        isFading = false;
        alphaColor = whiteDummy.GetComponent<SpriteRenderer>().material.color;
        alphaColor.a = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFading)
        {
            whiteDummy.GetComponent<SpriteRenderer>().material.color = Color.Lerp(whiteDummy.GetComponent<SpriteRenderer>().material.color, alphaColor, timeToFade * Time.deltaTime);
        }

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Collision is real");
        if (collision.gameObject.tag == "Player");
        {
            Debug.Log("Entered, Collision is happppepeeeeenenenenennened");
            isFading = true;
        }
    }


}
